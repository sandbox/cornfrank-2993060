<?php

namespace Drupal\multiple_select\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\multiple_select\MultipleSelectHelper;

/**
 * Plugin implementation of the 'multiple_select' widget.
 *
 * @FieldWidget(
 *   id = "multiple_select",
 *   label = @Translation("Multiple Select"),
 *   description = @Translation("Multiple select library field."),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   }
 * )
 */
class MultipleSelectWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return MultipleSelectHelper::defaultSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state)
      + MultipleSelectHelper::settingsForm($form_state, $this->getSettings());
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Standard select widget adds an _none item, this is not needed.
    unset($element['#options']['_none']);
    $element['#type'] = 'multiple_select';
    $element['#multiple_select'] = $this->getSettings();
    return $element;
  }

}
