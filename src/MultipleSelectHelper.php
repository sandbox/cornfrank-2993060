<?php

namespace Drupal\multiple_select;

use Drupal\Core\Form\FormStateInterface;

/**
 * Helper functions.
 */
class MultipleSelectHelper {

  /**
   * Return the default settings for the multiple select js library.
   *
   * @return array
   */
  public static function defaultSettings() {
    return [
      'name' => '',
      'isOpen' => FALSE,
      'placeholder' => '',
      'selectAll' => TRUE,
      'selectAllDelimiter' => ['[', ']'],
      'minimumCountSelected' => 3,
      'ellipsis' => FALSE,
      'multiple' => TRUE,
      'multipleWidth' => 80,
      'single' => FALSE,
      'filter' => FALSE,
      'width' => '100%',
      'dropWidth' => 30,
      'maxHeight' => 250,
      'container' => NULL,
      'position' => 'bottom',
      'keepOpen' => FALSE,
      'animate' => 'none',
      'displayValues' => FALSE,
      'delimiter' => ', ',
      'addTitle' => FALSE,
      'filterAcceptOnEnter' => FALSE,
      'hideOptionGroupCheckboxes' => FALSE,
      'selectAllText' => t('Select all'),
      'allSelected' => t('All selected'),
      'countSelected' => t('# of % selected'),
      'noMatchesFound' => t('No matches found.'),
    ];
  }

  /**
   * Return the form elements for editing the settings.
   *
   * This is used by the field widget, and can be used by other modules
   * to edit custom form settings.
   *
   * @param FormStateInterface $form_state
   * @param array $settings
   *  The initial settings values.
   * @return array
   */
  public static function settingsForm(FormStateInterface $form_state, array $settings) {
    $settings += self::defaultSettings();
    $elements = [];
    // I think this overrides the element name, so I ignored it.
//    $elements['name'] = [
//      '#title' => t('Name'),
//      '#type' => 'textfield',
//      '#size' => 30,
//      '#default_value' => $settings['name'],
//    ];

    $elements['isOpen'] = [
      '#title' => t('Is open'),
      '#description' => t('Start with the drop down select list open.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['isOpen'],
    ];

    $elements['keepOpen'] = [
      '#title' => t('Keep open'),
      '#description' => t('Drop down box stays open when element loses focus.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['keepOpen'],
    ];

    $elements['placeholder'] = [
      '#title' => t('Placeholder'),
      '#description' => t('The place holder text for the form element.'),
      '#type' => 'textfield',
      '#size' => 30,
      '#default_value' => $settings['placeholder'],
    ];

    $elements['selectAll'] = [
      '#title' => t('Include "select all" box'),
      '#description' => t(''),
      '#type' => 'checkbox',
      '#default_value' => $settings['selectAll'],
    ];

    // Select all delimiter ignored.

    $elements['minimumCountSelected'] = [
      '#title' => t('Minimum count selected'),
      '#description' => t('The number ofvalues to display in the box before using the "count selected text" message.'),
      '#type' => 'number',
      '#default_value' => $settings['minimumCountSelected'],
    ];

    $elements['ellipsis'] = [
      '#title' => t('Ellipsis'),
      '#description' => t('Display an ellipsis instead of the "count selected text" message.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['ellipsis'],
    ];

    $elements['multiple'] = [
      '#title' => t('Multiple'),
      '#description' => t('Multiple options per line in the drop down list.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['multiple'],
    ];

    $elements['multipleWidth'] = [
      '#title' => t('Multiple width'),
      '#description' => t('The width of the collumns in the drop down select list when multiple is on.'),
      '#type' => 'number',
      '#field_suffix' => 'px',
      '#default_value' => $settings['multipleWidth'],
    ];

    $elements['single'] = [
      '#title' => t('Single'),
      '#description' => t('Use radio buttons instead of checkboxes, thus only a single choice is allowed.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['single'],
    ];

    $elements['filter'] = [
      '#title' => t('Filter'),
      '#description' => t('Show a filter box for selections.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['filter'],
    ];

    $elements['filterAcceptOnEnter'] = [
      '#title' => t('Filter accept on enter'),
      '#description' => t('Not sure what this does.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['filterAcceptOnEnter'],
    ];

    $elements['width'] = [
      '#title' => t('Width'),
      '#description' => t('The width of the element. Requires the px/em/% suffix.'),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $settings['width'],
    ];

    $elements['dropWidth'] = [
      '#title' => t('Drop width'),
      '#description' => t('This seems to have no effect but needs to be a valid number.'),
      '#type' => 'number',
      '#min' => 1,
      '#default_value' => $settings['dropWidth'],
    ];

    $elements['maxHeight'] = [
      '#title' => t('Max height'),
      '#description' => t('The maximum height of the drop down selection box.'),
      '#type' => 'number',
      '#size' => 10,
      '#field_suffix' => 'px',
      '#default_value' => $settings['maxHeight'],
    ];

//    $elements['container'] = [
//      '#title' => t('Container'),
//      '#type' => '',
//      '#default_value' => $settings['container'],
//    ];

    $elements['position'] = [
      '#title' => t('Position'),
      '#description' => t('Where the selection box appears.'),
      '#type' => 'select',
      '#options' => [
        'top' => t('Top'),
        'bottom' => t('Bottom'),
      ],
      '#default_value' => $settings['position'],
    ];

    $elements['animate'] = [
      '#title' => t('Animate'),
      '#description' => t(''),
      '#type' => 'select',
      '#options' => [
        'none' => t('None'),
        'fade' => t('Fade'),
        'slide' => t('Slide'),
      ],
      '#default_value' => $settings['animate'],
    ];

    $elements['displayValues'] = [
      '#title' => t('Display values'),
      '#description' => t('Display the option values instead of the labels.'),
      '#type' => 'checkbox',
      '#default_value' => $settings['displayValues'],
    ];

    $elements['delimiter'] = [
      '#title' => t('Delimiter'),
      '#description' => t('Delimiter between the values displayed in the form.'),
      '#type' => 'textfield',
      '#size' => 10,
      '#default_value' => $settings['delimiter'],
    ];

    $elements['addTitle'] = [
      '#title' => t('Add title'),
      '#description' => t(''),
      '#type' => 'checkbox',
      '#default_value' => $settings['addTitle'],
    ];

    $elements['hideOptionGroupCheckboxes'] = [
      '#title' => t('Hide option group checkboxes'),
      '#description' => t(''),
      '#type' => 'checkbox',
      '#default_value' => $settings['hideOptionGroupCheckboxes'],
    ];

    $elements['selectAllText'] = [
      '#title' => t('Select all text'),
      '#description' => t(''),
      '#type' => 'textfield',
      '#default_value' => $settings['selectAllText'],
    ];

    $elements['allSelected'] = [
      '#title' => t('All selected text'),
      '#description' => t(''),
      '#type' => 'textfield',
      '#default_value' => $settings['allSelected'],
    ];

    $elements['countSelected'] = [
      '#title' => t('Count selected text'),
      '#description' => t(''),
      '#type' => 'textfield',
      '#default_value' => $settings['countSelected'],
    ];

    $elements['noMatchesFound'] = [
      '#title' => t('No matches found text'),
      '#description' => t(''),
      '#type' => 'textfield',
      '#default_value' => $settings['noMatchesFound'],
    ];

    return $elements;
  }
}
