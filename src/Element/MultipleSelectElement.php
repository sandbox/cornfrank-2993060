<?php

namespace Drupal\multiple_select\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;
use Drupal\multiple_select\MultipleSelectHelper;

/**
 * Provides a multiple select element.
 *
 * Additional properties:
 * - #multiple_select: Additional settings for the multiple select library.
 *   @see MultipleSelectHelper::defaultSettings() for the full list of
 *   settings and the default values.
 *
 * @FormElement("multiple_select")
 */
class MultipleSelectElement extends Select {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $class = get_class($this);
    $info['#multiple'] = TRUE;
    $info['#process'][] = [$class, 'processMultipleSelect'];
    $info['#multiple_select'] = [];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public static function preRenderSelect($element) {
    $element = parent::preRenderSelect($element);
    static::setAttributes($element, ['multiple-select-element']);
    return $element;
  }

  /**
   * Process the multiple_select element.
   *
   * @param array $element
   * @param FormStateInterface $form_state
   * @param array $complete_form
   * @return array
   */
  public static function processMultipleSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#attached']['library'][] = 'multiple_select/multiple_select';
    $element['#attached']['drupalSettings'] =
      $element['#multiple_select'] += MultipleSelectHelper::defaultSettings();
    return $element;
  }

}
