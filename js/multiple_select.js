(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.initMultipleSelect = {
    attach: function (context, settings) {
      $('.multiple-select-element', context).once('initMultipleSelect').multipleSelect(settings);
    }
  };

})(jQuery, Drupal);
