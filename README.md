## Introduction

Extends the standard select form element and options select field widget to
the multiple select js library so that multiple options can be selected 
using checkboxes.

## Requirements

The multi select js library: https://github.com/wenzhixin/multiple-select

## Installation

Download the [multiple select](https://github.com/wenzhixin/multiple-select) 
library, extract it to the libraries folder and rename the extracted folder 
"multiple-select". 

Enable the module in the usual way.

## Usage

For fields select the multiple select widget and set its options.

Form custom forms use the "multiple_select" form element and set the options
in the "#multiple_select" property.
